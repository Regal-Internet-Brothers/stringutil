stringutil
==========

Basic string utilities and general purpose string routines for the [Monkey programming language](https://github.com/blitz-research/monkey).

References / Special Thanks:
* [Pharmhaus's original adaptation](http://www.monkey-x.com/Community/posts.php?topic=9384) of [Vishal Chaudhary's "wild-card" implementation](http://www.geeksforgeeks.org/wildcard-character-matching).
